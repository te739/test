import pytest
from selene.support.shared import browser
from selene import be
import time


@pytest.fixture(autouse=True)
def setup():
    browser.open('https://google.com/')
    yield
    browser.quit()


def test_test():
    browser.element(
        '[spellcheck="false"]'
    ).set_value('test').press_enter()
    browser.element(
        '#logo > img'
    ).should(be.visible)
    time.sleep(5)
    browser.quit()

